from django.conf.urls import patterns, include, url
from django.contrib import admin
from stormProject import settings
from stormApp import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'stormProject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
    url(r'^stormApp/static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT}),
    url(r'^$', views.index, name='index'),
    url(r'^getFilmes/(?P<genero_id>.*)/(?P<ordem>.*)$', views.getFilmes, name='getFilmes'),
    url(r'^getFilme/(?P<id>.*)$', views.getFilme, name='getFilme'),
    url(r'^getGeneros/', views.getGeneros, name='getGeneros'),
    url(r'^getAtor/(?P<id>.*)$', views.getAtor, name='getAtor'),
    url(r'^getFilmesRelacionadosAtor/(?P<id>.*)$', views.getFilmesRelacionadosAtor, name='getFilmesRelacionadosAtor'),
    url(r'^getFilmesRelacionados/(?P<id>.*)$', views.getFilmesRelacionados, name='getFilmesRelacionados'),
)
