# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stormApp', '0006_auto_20151113_0452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='image',
            field=models.ImageField(default=b'actors/sem_foto.jpg', upload_to=b'actors/%Y/%m/%d', verbose_name=b'Foto'),
            preserve_default=True,
        ),
    ]
