# -*- coding: utf-8 -*-
from django.db import models
from collections import OrderedDict

class Actor(models.Model):
    name = models.CharField(verbose_name='Nome', max_length=200)
    image = models.ImageField(upload_to='actors/%Y/%m/%d',default='actors/sem_foto.jpg',verbose_name='Foto')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name='Ator'
        verbose_name_plural='Atores'
        ordering = ['name']


class Recommendation(models.Model):
    description = models.CharField(verbose_name=u'Descrição', max_length=200)
    year = models.IntegerField(default=0,verbose_name='Idade')
    def __str__(self):
        return self.description
    class Meta:
        verbose_name=u'Recomendação'
        verbose_name_plural='Recomendações'
        ordering = ['description']

class Genre(models.Model):
    name = models.CharField(verbose_name='Nome',max_length=20)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name=u'Gênero'
        verbose_name_plural=u'Gêneros'
        ordering = ['name']


class Movie(models.Model):
    title = models.CharField(verbose_name=u'Título original',max_length=200)
    title_pt = models.CharField(verbose_name=u'Título em protuguês',max_length=200)
    synopsis = models.TextField(verbose_name='Sinopse')
    duration = models.IntegerField(verbose_name=u'Duração em minutos')
    recommendation = models.ForeignKey( Recommendation,verbose_name=u'Recomendação')
    date = models.DateField(verbose_name=u'Data de lançamento')
    director = models.ForeignKey(Actor,verbose_name='Diretor')
    rate = models.FloatField(verbose_name='Nota',default=0)
    actors = models.ManyToManyField(Actor,related_name='actors', verbose_name='Atores')
    genres = models.ManyToManyField(Genre, verbose_name=u'Gêneros')
    image = models.ImageField(upload_to='movies/%Y/%m/%d',default='movies/sem_foto.png',verbose_name='Imagem')
    def getReacionados(self):
        filmes = {}
        for filme in Movie.objects.all().exclude(id=self.id):
            filmes[filme.id]=0
        for ator in self.actors.all():
            for filme in Movie.objects.filter(actors=ator.id).exclude(id=self.id):
                filmes[filme.id]+=1
        for gem in self.genres.all():
            for filme in Movie.objects.filter(genres=gem.id).exclude(id=self.id):
                filmes[filme.id]+=1
        return OrderedDict(sorted(filmes.items(),
                                  key=lambda kv: kv[1], reverse=True))
    def __str__(self):
        return self.title_pt
    class Meta:
        verbose_name='Filme'
        verbose_name_plural='Filmes'
        ordering = ['title_pt']




