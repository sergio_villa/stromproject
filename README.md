# README #

Essa é uma aplicação em Django para o recrutamento da StormSec

### Comentários sobre o desenvolvimento  ###

* O app StormApp foi feito em Django 1.7 e Python 2.7
* O banco de dados foi modelado utilizando a ORM do Django e MySQL
* O Admin foi feito utilizando o Grappelli
* Para o front foi utilizado o JQuery e o Bootstrap
* O layout foi criado com base nos layouts da StormSec (versão simplificada)
* Para garantir que o sistema fique leve (para mais de 5 mil usuário simultâneos) todo o front se utilizou de requisições Ajax
* Para as requisições Ajax o Django retorna versões simplificadas de seus models em Json
* Todas as funções JQuery e CSS necessários para o funcionamento da página são baixados somente no momento que a ágina é aberta pela primeira vez
* Para permitir que as páginas sejam compartilhadas, a ULR do sistema é atualizada o tempo todo com novas hashs. Assim o conteúdo pode ser compartilhado sem que uma nova requisição de download da página inteira seja feita. Exemplo de URL: stormApp.com/#filme=2  
* O admin foi levemente customizado para se adequar melhor ao modelo de dados