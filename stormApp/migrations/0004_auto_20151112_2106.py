# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stormApp', '0003_auto_20151111_1550'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movie',
            name='image_url',
        ),
        migrations.AddField(
            model_name='movie',
            name='image',
            field=models.ImageField(default=b'movies/sem_foto.png', upload_to=b'movies/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='image_url',
            field=models.CharField(max_length=250, verbose_name=b'Foto (camilho relativo)'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='assessments',
            field=models.IntegerField(verbose_name='Qtd. de Avalia\xe7\xf5es'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='date',
            field=models.DateField(verbose_name='Data de lan\xe7amento'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='duration',
            field=models.IntegerField(verbose_name='Dura\xe7\xe3o em minutos'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='genres',
            field=models.ManyToManyField(to='stormApp.Genre', verbose_name='G\xeaneros'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='recommendation',
            field=models.ForeignKey(verbose_name='Recomenda\xe7\xe3o', to='stormApp.Recommendation'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='title',
            field=models.CharField(max_length=200, verbose_name='T\xedtulo original'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='title_pt',
            field=models.CharField(max_length=200, verbose_name='T\xedtulo em protugues'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recommendation',
            name='description',
            field=models.CharField(max_length=200, verbose_name='Descri\xe7\xe3o'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recommendation',
            name='image_url',
            field=models.CharField(max_length=200, verbose_name=b'Imagem (caminho relativo)'),
            preserve_default=True,
        ),
    ]
