# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stormApp', '0004_auto_20151112_2106'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actor',
            name='image_url',
        ),
        migrations.RemoveField(
            model_name='recommendation',
            name='image_url',
        ),
        migrations.AddField(
            model_name='actor',
            name='image',
            field=models.ImageField(default=b'actors/sem_foto.png', upload_to=b'actors/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recommendation',
            name='image',
            field=models.ImageField(default=b'recs/sem_foto.png', upload_to=b'recs/%Y/%m/%d'),
            preserve_default=True,
        ),
    ]
