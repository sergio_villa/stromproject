# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stormApp', '0002_auto_20151111_1520'),
    ]

    operations = [
        migrations.RenameField(
            model_name='movie',
            old_name='tittle',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='movie',
            old_name='tittle_pt',
            new_name='title_pt',
        ),
    ]
