# -*- coding: utf-8 -*-
from django.shortcuts import render
import json
from django.http import HttpResponse
from django.http import Http404
from .models import Actor,Movie,Recommendation,Genre

def index(request):
    context =  {
        'genero':u'Gêneros'
    }
    return render(request,'stormApp/index.html',context)

def getFilmes(request,genero_id,ordem):
    rodem_str = 'title_pt'
    if(ordem == '-1'):
        rodem_str = '-title_pt'
    if(genero_id == 'none'):
        movies = Movie.objects.order_by(rodem_str).all()
    else:
        movies = Movie.objects.filter(genres=genero_id).order_by(rodem_str).all()
    data = []
    for movie in movies:
        data.append(
            {'title_pt':movie.title_pt, 'img_url':unicode(movie.image), 'rate':movie.rate, 'id':movie.id}
        )
    return HttpResponse(json.dumps(data), content_type="application/json")

def getGeneros(request):
    generos = Genre.objects.all()
    data = []
    for genero in generos:
        data.append(
            {'name':genero.name, 'id':genero.id}
        )
    return HttpResponse(json.dumps(data), content_type="application/json")

def getFilme(request,id):
    movie = Movie.objects.get(id=id)
    actors = []
    for acs in movie.actors.all():
        actors.append(
            {'name':acs.name, 'id':acs.id}
        )
    genres = []
    for gens in movie.genres.all():
        genres.append(
            {'name':gens.name, 'id':gens.id}
        )
    data = {'title_pt':movie.title_pt, 'img_url':unicode(movie.image), 'rate':movie.rate,
            'title':movie.title,'synopsis':movie.synopsis,'duration':movie.duration, 'recommendation_year':movie.recommendation.year,
            'recommendation':movie.recommendation.description,'date':movie.date.year,
            'director_name':movie.director.name,'director_id':movie.director.id,'actors':actors,'genres':genres}

    return HttpResponse(json.dumps(data), content_type="application/json")

def getFilmesRelacionadosAtor(request,id):
    movies = Movie.objects.filter(actors=id)[:20]
    data = []
    for movie in movies:
        data.append(
            {'title_pt':movie.title_pt, 'img_url':unicode(movie.image), 'rate':movie.rate, 'id':movie.id}
        )
    return HttpResponse(json.dumps(data), content_type="application/json")

def getFilmesRelacionados(request,id):
    movies = Movie.objects.get(id=id).getReacionados()
    data = []
    counter = 0;
    for item in movies:
        counter +=1
        if(counter > 10):
            break
        if(movies[item] >0):
            movie = Movie.objects.get(id=item)
            data.append(
                {'title_pt':movie.title_pt, 'img_url':unicode(movie.image), 'rate':movie.rate, 'id':movie.id}
            )

    return HttpResponse(json.dumps(data), content_type="application/json")

def getAtor(request,id):
    ator = Actor.objects.get(id=id)
    data = (
            {'name':ator.name, 'id':ator.id,'img_url':unicode(ator.image)}
        )
    return HttpResponse(json.dumps(data), content_type="application/json")