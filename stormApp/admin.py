from django.contrib import admin

from .models import Movie,Actor,Recommendation,Genre


class MovieOptions(admin.ModelAdmin):
    def imagem(self,obj):
        return u"<img style='max-height: 150px;' src='/media/%s' />" % obj.image
    imagem.allow_tags = True
    def atores(self,obj):
        atores = obj.actors.all()
        saida = ''
        for ator in atores:
            saida += '<a href="../actor/'+str(ator.id)+'/">'+str(ator)+'</a>, '
        return saida
    atores.allow_tags = True
    def generos(self,obj):
        generos = obj.genres.all()
        saida = ''
        for genero in generos:
            saida += '<a href="../genre/'+str(genero.id)+'/">'+str(genero)+'</a>, '
        return saida
    generos.allow_tags = True

    list_display = ('title_pt', 'title', 'date','recommendation', 'director','atores','generos','synopsis','duration','rate', 'imagem')

class ActorOptions(admin.ModelAdmin):
    def imagem(self,obj):
        return u"<img style='max-height: 150px;' src='/media/%s' />" % obj.image
    imagem.allow_tags = True
    def filmes(self,obj):
        filmes = Movie.objects.filter(actors=obj.id)
        saida = ''
        for filme in filmes:
            saida += '<a href="../movie/'+str(filme.id)+'/">'+str(filme)+'</a>, '
        return saida
    filmes.allow_tags = True

    list_display = ('name', 'filmes', 'imagem')

class RecommendationOptions(admin.ModelAdmin):
    list_display = ('description', 'year')


admin.site.register(Movie,MovieOptions)
admin.site.register(Actor,ActorOptions)
admin.site.register(Recommendation,RecommendationOptions)
admin.site.register(Genre)