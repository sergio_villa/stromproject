# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stormApp', '0005_auto_20151112_2127'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='actor',
            options={'ordering': ['name'], 'verbose_name': 'Ator', 'verbose_name_plural': 'Atores'},
        ),
        migrations.AlterModelOptions(
            name='genre',
            options={'ordering': ['name'], 'verbose_name': 'G\xeanero', 'verbose_name_plural': 'G\xeaneros'},
        ),
        migrations.AlterModelOptions(
            name='movie',
            options={'ordering': ['title_pt'], 'verbose_name': 'Filme', 'verbose_name_plural': 'Filmes'},
        ),
        migrations.AlterModelOptions(
            name='recommendation',
            options={'ordering': ['description'], 'verbose_name': 'Recomenda\xe7\xe3o', 'verbose_name_plural': 'Recomenda\xe7\xf5es'},
        ),
        migrations.RemoveField(
            model_name='movie',
            name='assessments',
        ),
        migrations.RemoveField(
            model_name='recommendation',
            name='image',
        ),
        migrations.AddField(
            model_name='recommendation',
            name='year',
            field=models.IntegerField(default=0, verbose_name=b'Idade'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='image',
            field=models.ImageField(default=b'actors/sem_foto.png', upload_to=b'actors/%Y/%m/%d', verbose_name=b'Foto'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='image',
            field=models.ImageField(default=b'movies/sem_foto.png', upload_to=b'movies/%Y/%m/%d', verbose_name=b'Imagem'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='rate',
            field=models.FloatField(default=0, verbose_name=b'Nota'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='movie',
            name='title_pt',
            field=models.CharField(max_length=200, verbose_name='T\xedtulo em protugu\xeas'),
            preserve_default=True,
        ),
    ]
