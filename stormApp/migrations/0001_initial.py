# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Nome')),
                ('image_url', models.CharField(max_length=250, verbose_name=b'Camilho relativo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tittle', models.CharField(max_length=200, verbose_name=b'Titulo original')),
                ('tittle_pt', models.CharField(max_length=200, verbose_name=b'Titulo em protugues')),
                ('synopsis', models.TextField(verbose_name=b'Sinopse')),
                ('duration', models.IntegerField(verbose_name=b'Duracao em minutos')),
                ('date', models.DateField(verbose_name=b'Data de lacamento')),
                ('image_url', models.CharField(max_length=250, verbose_name=b'Caminho relativo')),
                ('rate', models.FloatField(verbose_name=b'Nota')),
                ('assessments', models.IntegerField(verbose_name=b'Qtd. de Avaliacoes')),
                ('director', models.ForeignKey(verbose_name=b'Diretor', to='stormApp.Actor')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recommendation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=200, verbose_name=b'Descricao')),
                ('image_url', models.CharField(max_length=200, verbose_name=b'Caminho relativo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='movie',
            name='recommendation',
            field=models.ForeignKey(verbose_name=b'Recomendacao', to='stormApp.Recommendation'),
            preserve_default=True,
        ),
    ]
