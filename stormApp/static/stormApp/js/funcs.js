        var Filme="";
        var Ator="";
        var Genero='none';

function getFilmes(genero,ordem){
        if(genero != 'none')Genero = genero;
        if(genero == 'all')Genero = 'none';
        $.getJSON( "/getFilmes/"+Genero+"/"+ordem, function( data ) {
          if(Genero != 'none'){
                $('#titulo_lista').html($('#genero'+genero).html());
            }else{
                $('#titulo_lista').html('Listagem de filmes');
            }
          $('#filmes_thumbnails').html('');
          $('#lista_ordem_seletor').removeClass('hidden');
          $('#detalhe_filmes_container').addClass('hidden');
          $('#ator_container').addClass('hidden');
          $.each( data, function( i, item ) {

             var star1 = item.rate>=1?'':'-empty';
             var star2 = item.rate>=2?'':'-empty';
             var star3 = item.rate>=3?'':'-empty';
             var star4 = item.rate>=4?'':'-empty';
             var star5 = item.rate>=5?'':'-empty';
             var class_str = "";
             if(i%5==0)class_str="col-md-offset-1";
             var thumbnail = '<div class="col-sm-6 col-md-2 '+class_str+'" onclick="getFilme('+item.id+');">'+
                             '   <div class="thumbnail">'+
                             '     <div class="caption">'+
                             '       <span class="small">'+item.title_pt+'</span>'+
                             '     </div>'+
                             '     <img src="media/'+item.img_url+'"/>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '   </div>'+
                             '</div>';
             $('#filmes_thumbnails').append(thumbnail);

          });
          Filme = "";
          Ator = "";
          updateUrl();
        });
}

function getGeneros(){
        $.getJSON( "/getGeneros", function( data ) {
          $.each( data, function( i, item ) {
             var thumbnail = '<li><a href="#" id="genero'+item.id+'" onclick="getFilmes(\''+item.id+'\',1);">'+item.name+'</a></li>';
             $('#select_genero').append(thumbnail);

          });
           Filme = getUrlParameter('filme');
            Ator = getUrlParameter('ator');
            Genero = getUrlParameter('genero');
            if(Filme != ""){
                getFilme(Filme);
                return;
            }
            if(Genero != ""){
                getFilmes(Genero,1);
                return;
            }
            if(Ator != ""){
                getAtor(Ator);
                return;
            }

            getFilmes('none',1);
        });
}

function getFilme(id){
        $.getJSON( "/getFilme/"+id, function( data ) {
            $('#detalhe_filme_diretor').html('<a hfer="#" onclick="getAtor('+data.director_id+');">'+data.director_name+'</a>');
            $('#detalhe_filme_titulo').html(data.title_pt);
            $('#detalhe_filme_duracao').html(data.duration+' min.');
            $('#detalhe_filme_titulo_original').html(data.title+' ('+data.date+')');
            $('#detalhe_filme_sinopse').html(data.synopsis);
            var rec = 'btn-warning';
            if(data.recommendation_year==0){
                rec = 'btn-success';
                data.recommendation_year = 'L';
            }
            if(data.recommendation_year==18)rec = 'btn-danger';
            $('#detalhe_filme_recomendacao').html(
                     '<button style="margin-right: 3%;" type="button" class="btn '+rec+' noBut">'+
                      data.recommendation_year+
                     '</button>'+
                      data.recommendation);
              $('#detalhe_filme_elenco').html('');
              $.each( data.actors, function( i, item ) {
                    $('#detalhe_filme_elenco').append('<a hfer="#" onclick="getAtor('+item.id+');">'+item.name+'</a>');
                    $('#detalhe_filme_elenco').append(', ');
              });
              $('#detalhe_filme_generos').html('');
              $.each( data.genres, function( i, item ) {
                    $('#detalhe_filme_generos').append('<a hfer="#" onclick="getFilmes('+item.id+',1);">'+item.name+'</a>');
                    $('#detalhe_filme_generos').append(', ');
              });
              var star1 = data.rate>=1?'':'-empty';
              var star2 = data.rate>=2?'':'-empty';
              var star3 = data.rate>=3?'':'-empty';
              var star4 = data.rate>=4?'':'-empty';
              var star5 = data.rate>=5?'':'-empty';
              $('#detalhe_filme_rate').html(
                    '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                    '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                    '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                    '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                    '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'
              );
              $('#detalhe_filme_img').attr('src','media/'+data.img_url);
              $('#detalhe_filmes_container').removeClass('hidden');
              $('#ator_container').addClass('hidden');
              getFilmesRelacionados(id);
              Filme = id;
              Genero = "";
              Ator = "";
              updateUrl();
        });
}
function getAtor(id){
        $.getJSON( "/getAtor/"+id, function( data ) {
              $('#detalhe_ator_nome').html(data.name);
              $('#ator_img').attr('src','media/'+data.img_url);
              $('#detalhe_filmes_container').addClass('hidden');
              $('#ator_container').removeClass('hidden');
              getFilmesRelacionadosAtor(id);
        });
}

function getFilmesRelacionadosAtor(id){
        $.getJSON( "/getFilmesRelacionadosAtor/"+id, function( data ) {
          $('#filmes_thumbnails').html('');
          $('#titulo_lista').html('Filmes em que atuou');
          $('#lista_ordem_seletor').addClass('hidden');
          $.each( data, function( i, item ) {

             var star1 = item.rate>=1?'':'-empty';
             var star2 = item.rate>=2?'':'-empty';
             var star3 = item.rate>=3?'':'-empty';
             var star4 = item.rate>=4?'':'-empty';
             var star5 = item.rate>=5?'':'-empty';
             var class_str = "";
             if(i%5==0)class_str="col-md-offset-1";
             var thumbnail = '<div class="col-sm-6 col-md-2 '+class_str+'" onclick="getFilme('+item.id+');">'+
                             '   <div class="thumbnail">'+
                             '     <div class="caption">'+
                             '       <span class="small">'+item.title_pt+'</span>'+
                             '     </div>'+
                             '     <img src="media/'+item.img_url+'"/>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '   </div>'+
                             '</div>';
             $('#filmes_thumbnails').append(thumbnail);

          });
          Filme = "";
          Genero = "";
          Ator = id;
          updateUrl();
        });
}
function getFilmesRelacionados(id){
        $.getJSON( "/getFilmesRelacionados/"+id, function( data ) {
          $('#filmes_thumbnails').html('');
          $('#titulo_lista').html('Filmes Relacionados');
          $('#lista_ordem_seletor').addClass('hidden');
          $.each( data, function( i, item ) {

             var star1 = item.rate>=1?'':'-empty';
             var star2 = item.rate>=2?'':'-empty';
             var star3 = item.rate>=3?'':'-empty';
             var star4 = item.rate>=4?'':'-empty';
             var star5 = item.rate>=5?'':'-empty';
             var class_str = "";
             if(i%5==0)class_str="col-md-offset-1";
             var thumbnail = '<div class="col-sm-6 col-md-2 '+class_str+'" onclick="getFilme('+item.id+');">'+
                             '   <div class="thumbnail">'+
                             '     <div class="caption">'+
                             '       <span class="small">'+item.title_pt+'</span>'+
                             '     </div>'+
                             '     <img src="media/'+item.img_url+'"/>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '<span class="glyphicon glyphicon-star'+star1+'" aria-hidden="true"></span>'+
                             '   </div>'+
                             '</div>';
             $('#filmes_thumbnails').append(thumbnail);

          });
        });
}
function updateUrl(){
        var hashString = "";
        if(Filme !="")hashString = 'filme='+Filme;
        if(Genero !="")hashString = 'genero='+Genero;
        if(Ator !="")hashString = 'ator='+Ator;
        location.hash = hashString;
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = location.hash;
    sPageURL = sPageURL.replace("#","");
    var sURLVariables = sPageURL.split('&');
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    return "";
};

function load(){
        getGeneros();
}